FROM upstreamtech/python-geo-base:v7

WORKDIR /usr/src/

ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal

ADD . /usr/src/python-fmask/

RUN apt-get update && \
    apt-get install -y gdal-bin && \
    pip install --no-cache-dir --upgrade rasterio==1.0a9 GDAL==1.10.0 kafka-python==1.3.3 && \
    cd python-fmask && \
    python setup.py build && \
    python setup.py install && \
    cd ../ && \
    wget https://bitbucket.org/chchrsc/rios/downloads/rios-1.4.3.tar.gz && \
    tar xfz rios-1.4.3.tar.gz && \
    cd rios-1.4.3 && \
    python setup.py install --prefix=/usr/src/rios && \
    cd ../ && \
    rm -rf rios-*

ENV PATH="/usr/src/rios/bin:$PATH"
ENV PYTHONPATH="/usr/src/rios/lib/python3.5/site-packages:$PYTHONPATH"
