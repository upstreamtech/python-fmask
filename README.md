Implementation in Python of the cloud and shadow algorithms known collectively as Fmask,
as published in:

Zhu, Z. and Woodcock, C.E. (2012). Object-based cloud and cloud shadow detection in Landsat imagery Remote Sensing of Environment 118 (2012) 83-94.

and

Zhu, Z., Wang, S. and Woodcock, C.E. (2015). Improvement and expansion of the Fmask algorithm: cloud, cloud shadow, and snow detection for Landsats 4-7, 8, and Sentinel 2 images Remote Sensing of Environment 159 (2015) 269-277.

Please visit the main web page at: [pythonfmask.org](http://pythonfmask.org/)

## Upstream Tech Modifications
1. Added a Dockerfile
2. Changed uses of tempfile.mkstemp in fmask/fmask.py to use a `make_temp_path` function which emulates that function but without the "secure" parts which were causing problems.

### Development
You don't want to install fmask locally, it's a pain. Instead, build the docker image, mount your code inside and run it from within the container.

```
docker build -f Dockerfile . # the dot is required, specifies current directory.
```

Then do something like below, using the image id printed out after buildling the image and a path to wherever satellite data is on your computer.
```
docker run -it -v (pwd)/fmask/fmask.py:/usr/local/lib/python3.5/site-packages/fmask/fmask.py -v /Users/alden/code/upstream/irrigation-classifier/data/download-cache/S2/S2A_OPER_MSI_L1C_TL_MPS__20160803T235100_A005830_T10TFQ_N02.04:/granule <image id> bash
```

### Deploying

Build and tag and then push. Make sure to bump the version part (`:v0`).
``
docker build -f Dockerfile -t gcr.io/monitron-dev/python-geo-base-with-fmask:v0 .
gcloud docker -- push gcr.io/monitron-dev/python-geo-base-with-fmask:v0
```
